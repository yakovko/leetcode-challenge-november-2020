package main

import "fmt"

func main() {
	n := 6
	edges := [][]int{[]int{3, 0}, []int{3, 1}, []int{3, 2}, []int{3, 4}, []int{5, 4}}
	findMinHeightTrees(n, edges)
}

func findMinHeightTrees(n int, edges [][]int) []int {
	heights := make(map[int][]int)
	minH := n
	var exV []int
	for i := 0; i < n; i++ {
		if contains(exV, i) {
			continue
		}
		longestPath := getLongestPath([]int{i}, edges)
		height := len(longestPath) - 1
		if height > minH {
			head := longestPath[:len(longestPath)-minH - 1]
			tail := longestPath[minH + 1:]
			exV = merge(exV, head)
			exV = merge(exV, tail)
		} else {
			minH = height
			heights[height] = append(heights[height], i)
		}
	}
	fmt.Printf("Min height roots: %v", heights[minH])
	return heights[minH]
}

func getLongestPath(visited []int, edges [][]int) []int {
	lastVisited := visited[len(visited)-1]
	maxPath := visited
	for _, edge := range edges {
		if !contains(edge, lastVisited) {
			continue
		}
		next := edge[0]
		if next == lastVisited {
			next = edge[1]
		}
		if !contains(visited, next) {
			nextPath := getLongestPath(append(visited, next), edges)
			if len(maxPath) < len(nextPath) {
				maxPath = nextPath
			}
		}
	}
	return maxPath
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func merge(aa []int, ab []int) []int {
	for _, b := range ab {
		if !contains(aa, b) {
			aa = append(aa, b)
		}
	}
	return aa
}
