package main

import "fmt"

func main() {
	input := [][]int{{1, 1, 0, 0}, {1, 0, 0, 1}, {0, 1, 1, 1}, {1, 0, 1, 0}}
	fmt.Println(flipAndInvertImage(input))
}

func flipAndInvertImage(A [][]int) [][]int {
	not := func(x int) int {
		if x == 0 {
		return 1
	}
		return 0
	}

	lr := len(A[0])
	for _, r := range A {
		for i, j := 0, lr-1; i <= j; i, j = i+1, j-1 {
			if i == j {
				r[i] = not(r[i])
				break
			}
			r[i], r[j] = not(r[j]), not(r[i])
		}
	}
	return A
}
