package main

import (
	"fmt"
	"math"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func makeTree(input []int) *TreeNode {
	root := &TreeNode{input[0], nil, nil}
	i := 1
	nodes := []*TreeNode{root}
	for {
		if len(nodes) == 0 {
			break
		}
		nextNodes := []*TreeNode{}
		for j, in := i, 0; j < i+len(nodes)*2; j, in = j+2, in+1 {
			if j >= len(input) {
				break
			}
			if input[j] != -1 {
				leftNode := &TreeNode{input[j], nil, nil}
				nodes[in].Left = leftNode
				nextNodes = append(nextNodes, leftNode)
			}
			if input[j+1] != -1 {
				rightNode := &TreeNode{input[j+1], nil, nil}
				nodes[in].Right = rightNode
				nextNodes = append(nextNodes, rightNode)
			}
		}
		i += len(nodes) * 2
		nodes = nextNodes
	}
	return root
}

func main() {
	// input := []int{16, 2, 13, 6, -1, 3, 7, 10, 9, 20, 8, -1, 23, 17, -1, -1, -1, 5, 22, 1, 4, -1, -1, 18, -1, 21, 14, -1, -1, -1, -1, 11, -1, 12, -1, -1, -1, 24, 19, -1, 0, -1, 15}
	input := []int{2, -1, 0, 1, -1}
	var root *TreeNode
	root = makeTree(input)
	fmt.Println(maxAncestorDiff(root))
}

type TreeNodeExt struct {
	Val   int
	DMin  int
	DMax  int
	Left  *TreeNodeExt
	Right *TreeNodeExt
}

func maxAncestorDiff(root *TreeNode) int {
	rootX := getExtra(root)
	return getMaxDiff(rootX)
}

func getMaxDiff(root *TreeNodeExt) int {
	maxDiff := Max(Abs(root.Val-root.DMin), Abs(root.Val-root.DMax))
	if root.Left != nil {
		maxDiff = Max(maxDiff, getMaxDiff(root.Left))
	}
	if root.Right != nil {
		maxDiff = Max(maxDiff, getMaxDiff(root.Right))
	}
	return maxDiff
}

func getExtra(root *TreeNode) *TreeNodeExt {
	lmin := math.MaxInt32
	lmax := math.MinInt32
	rmin := math.MaxInt32
	rmax := math.MinInt32

	if root.Val == 15 {
		fmt.Println("here")
	}

	var lextra, rextra *TreeNodeExt
	if root.Left != nil {
		lextra = getExtra(root.Left)
		lmin = Min(lextra.DMin, lextra.Val)
		lmax = Max(lextra.DMax, lextra.Val)
	}

	if root.Right != nil {
		rextra = getExtra(root.Right)
		rmin = Min(rextra.Val, rextra.DMin)
		rmax = Max(rextra.Val, rextra.DMax)
	}

	if root.Right == nil && root.Left == nil {
		lmin = root.Val
		lmax = root.Val
	}

	return &TreeNodeExt{
		Val:   root.Val,
		DMax:  Max(lmax, rmax),
		DMin:  Min(lmin, rmin),
		Left:  lextra,
		Right: rextra,
	}
}

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
