package main

/**
 * https://leetcode.com/explore/challenge/card/november-leetcoding-challenge/564/week-1-november-1st-november-7th/3518/
 */

import (
	"fmt"
)

func main() {
	s := "leetcode"
	sPower := 2
	fmt.Printf("%d == %d", maxPower(s), sPower)
}

func maxPower(s string) int {
	maxP := 1
	prev := 'A'
	counter := 1
	for _, ch := range s {
		if ch == prev {
			counter++
			if counter > maxP {
				maxP = counter
			}
		} else {
			counter = 1
		}
		prev = ch
	}
	return maxP
}