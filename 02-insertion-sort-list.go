package main

import (
	"fmt"
	"strconv"
)

/**
 * https://leetcode.com/explore/challenge/card/november-leetcoding-challenge/564/week-1-november-1st-november-7th/3517/
 */

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	list := &ListNode{4, &ListNode{ 2, &ListNode{ 3, &ListNode{1, &ListNode{5, nil}}}}}
	printList(list)
	printList(insertionSortList(list))
}

func insertionSortList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	var sortedListHead *ListNode = &ListNode{ head.Val, nil }
	head = head.Next
	for head != nil {
		value := head.Val
		current := &sortedListHead
		for current != nil {
			next := (*current).Next

			if (*current).Val >= value {
				sortedListHead = &ListNode { value, sortedListHead }
				break
			}

			if next == nil {
				newElem := &ListNode { value, nil }
				(*current).Next = newElem
				break
			}

			if (*next).Val >= value {
				newNode := &ListNode{ value, next }
				(*current).Next = newNode
				break
			}

			current = &(*current).Next
		}
		head = head.Next
	}
	return sortedListHead
}

func printList(head *ListNode) {
	str := "[ "
	for head != nil {
		str += strconv.Itoa(head.Val)
		if head.Next != nil {
			str += ", "
		}
		head = head.Next
	}
	fmt.Println(str + " ]")
}
